package mi.web.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import mi.dto.demo.StudentDTO;
import mi.model.demo.Student;
import mi.web.BaseActionImpl;

@Controller
@RequestMapping(value="demo/student")
public class StudentActionImpl extends BaseActionImpl<Student, StudentDTO> implements
		IStudentAction {

	@Override
	public String content() {
		return null;
	}

}
