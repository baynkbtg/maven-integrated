package mi.model.demo;

import java.io.Serializable;

import mi.model.BaseModel;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月26日 上午8:55:08 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 学生实体类 ]
 */
public class Student extends BaseModel implements Serializable {

	private static final long serialVersionUID = 9141353974998922524L;

	private String name;//姓名
	private Integer age;//年龄
	
	public Student(){
		
	}
	
	public Student(Integer _id){
		this.id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

}
