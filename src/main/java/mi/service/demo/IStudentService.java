package mi.service.demo;

import mi.dto.demo.StudentDTO;
import mi.model.demo.Student;
import mi.service.IBaseService;

public interface IStudentService extends IBaseService<Student, StudentDTO> {

}
