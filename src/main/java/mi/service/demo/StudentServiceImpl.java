package mi.service.demo;

import mi.dto.demo.StudentDTO;
import mi.model.demo.Student;
import mi.service.BaseServiceImpl;

import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl extends BaseServiceImpl<Student, StudentDTO> implements
		IStudentService {

}
