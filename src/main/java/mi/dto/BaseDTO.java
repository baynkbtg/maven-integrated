package mi.dto;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:17:18 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ DTO用于搜索 ]
 */
public abstract class BaseDTO extends PageableDTO
		implements Serializable, Pageable {

	private static final long serialVersionUID = -5958525794931360435L;


}
