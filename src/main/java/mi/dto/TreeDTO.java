package mi.dto;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * @ProjectName: [ maven 自动化集成测试系统 ]
 * @Author: [ huanghuanlai ]
 * @CreateTime: [ 2015年5月25日 下午7:17:44 ]
 * @Copy: [ dounine.com ]
 * @Version: [ V1.0 ]
 * @Description: [ 树形DTO ]
 */
@JsonIgnoreProperties(value={"hibernateLazyInitializer","handler","fieldHandler"})//防止hibernate懒加载与jackson造成的死循环 
public class TreeDTO<Model extends Serializable> extends BaseDTO implements Serializable {

	private static final long serialVersionUID = -1916045024404285352L;

	protected String state = null;// 树形状态
	protected Boolean expand = false;
	public static final String OPEN = "open";// 展开
	public static final String CLOSED = "closed";// 关闭
	public Model parent = null;// 父级
	public List<Model> children;// 子级

	public void setState(Object state) {
		if (state == null) {
			this.state = null;
		} else {
			if (state.equals(OPEN)) {
				this.state = OPEN;
			} else {
				this.state = CLOSED;
			}
		}
	}

	public Boolean getExpand() {
		return expand;
	}

	public List<Model> getChildren() {
		return children;
	}

	public Model getParent() {
		return parent;
	}

	public void setParent(Model parent) {
		this.parent = parent;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public void setChildren(List<Model> children) {
		this.children = children;
	}

	public void setExpand(Boolean expand) {
		this.expand = expand;
	}
}
